const Data = [
  {
    nama: "Ojan",
    alamat: "Citayam",
    jenis_kelamin: "Pria"
  },
  {
    nama: "Rara",
    alamat: "Pondok Rajeg",
    jenis_kelamin: "Perempuan"
  },
  {
    nama: "Dinda",
    alamat: "Citeureup",
    jenis_kelamin: "Perempuan"
  },
  {
    nama: "Pajit",
    alamat: "Pasar Minggu",
    jenis_kelamin: "Laki-Laki"
  }
];

export default Data;
