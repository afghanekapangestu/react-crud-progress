import React from "react";
// import logo from "./logo.svg";
import "bootstrap/dist/css/bootstrap.min.css";
import { Navbar, NavbarBrand, Jumbotron, Row, Col } from "reactstrap";
import Table from "./Table";
import Data from "./Data";
import FormInput from "./FormInput";
import axios from "axios";
import { thisExpression } from "@babel/types";

class App extends React.Component {
  // Stateful Component
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      dataEdit: null
    };
  }
  componentDidMount() {
    this.load();
  }

  load() {
    axios
      .get("http://localhost:5700/data")
      .then(response => {
        console.log(response);
        this.setState({
          data: response.data
        });
      })
      .catch(e => {
        console.log(e);
        alert("Error");
      });
  }
  render() {
    return (
      <React.Fragment>
        <Navbar color="light" light>
          <NavbarBrand>Menu</NavbarBrand>
        </Navbar>
        <Row>
          <Col md="4">
            <FormInput
              dataEdit={this.state.dataEdit}
              onUpdate={(id, data) => {
                axios
                  .put(`http://localhost:5700/data/${id}`, data)
                  .then(resp => {
                    if (resp.status == 202) {
                      this.load();
                    }
                  })
                  .catch(e => {
                    console.log(e);
                    alert(e);
                  });
              }}
              onSave={newdata => {
                axios
                  .post("http://localhost:5700/newdata", {
                    nama: newdata.nama,
                    alamat: newdata.alamat,
                    jk: newdata.jenis_kelamin
                  })
                  .then(response => {
                    if (response.status === 201) {
                      // Disini harusnya reload data
                      this.load();
                    }
                  })
                  .catch(e => {
                    alert("Error" + e);
                    console.log(e);
                  });
              }}
            />
          </Col>

          <Col md="8">
            {/* <Jumbotron>
              <h1 className="display-3">Afghan</h1>
              <p className="lead">Perum. Bukit Waringin</p>
              <hr />
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Incidunt alias minus nihil numquam reiciendis hic, quisquam
                eveniet perferendis eaque, rem dignissimos repellendus! Eligendi
                aliquid voluptatibus praesentium tenetur, modi ipsam nesciunt?
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Atque
                quia ratione suscipit, recusandae officiis fuga? Vel voluptas
                veritatis magni fugiat deleniti, saepe repellendus sunt, ex
                facilis cumque, velit eveniet culpa.
              </p>
              <p className="lead">
                <button
                  className="btn btn-primary"
                  onClick={e => {
                    this.setState({
                      data: Data
                    });
                  }}
                >
                  Halo
                </button>
              </p>
            </Jumbotron> */}
            <Table
              onSelectEdit={data => {
                this.setState({
                  dataEdit: data
                });
              }}
              nama="Afghan Eka Pangestu"
              data={this.state.data}
              onDelete={id => {
                // var data = this.state.data;
                // data.splice(index, 1);
                // this.setState({
                //   data: data
                // });
                axios
                  .delete(`http://localhost:5700/data/${id}`)
                  .then(resp => {
                    if (resp.status == 202) {
                      this.load();
                      this.setState({
                        dataEdit: null
                      });
                    }
                  })
                  .catch(e => {
                    console.log(e);
                    alert(e);
                  });
              }}
            />
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

export default App;
