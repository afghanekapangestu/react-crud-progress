import React from "react";
import { Container, Row, Col } from "reactstrap";

class FormInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nama: "",
      alamat: "",
      jenis_kelamin: ""
    };
  }
  componentWillReceiveProps(propsbaru) {
    if (propsbaru.dataEdit != null) {
      this.setState({
        edit: true,
        nama: propsbaru.dataEdit.nama,
        alamat: propsbaru.dataEdit.alamat,
        jenis_kelamin: propsbaru.dataEdit.jenis_kelamin
      });
    } else {
      this.setState({
        edit: false
      });
    }
  }
  save() {
    if (this.state.edit == true) {
      this.props.onUpdate(this.props.dataEdit.id, {
        nama: this.state.nama,
        alamat: this.state.alamat,
        jk: this.state.jenis_kelamin
      });
    } else {
      this.props.onSave({
        nama: this.state.nama,
        alamat: this.state.alamat,
        jenis_kelamin: this.state.jenis_kelamin
      });
    }
    this.setState({
      nama: "",
      alamat: "",
      jenis_kelamin: "",
      edit: false
    });
  }
  render() {
    return (
      <Container>
        <Row>
          <Col>
            {this.state.edit && <h2>Mode Edit</h2>}
            <div className="form-group">
              <label htmlFor="">Nama</label>
              <input
                type="text"
                name=""
                id=""
                value={this.state.nama}
                className="form-control"
                onChange={e => {
                  this.setState({
                    nama: e.target.value
                  });
                }}
              />
            </div>
            <div className="form-group">
              <label htmlFor="">Alamat</label>
              <input
                type="text"
                name=""
                id=""
                value={this.state.alamat}
                className="form-control"
                onChange={e => {
                  this.setState({
                    alamat: e.target.value
                  });
                }}
              />
            </div>
            <div className="form-group">
              <label htmlFor="">Jenis Kelamin</label>
              <input
                type="text"
                name=""
                id=""
                value={this.state.jenis_kelamin}
                className="form-control"
                onChange={e => {
                  this.setState({
                    jenis_kelamin: e.target.value
                  });
                }}
              />
            </div>
            <button
              className="btn btn-primary"
              onClick={e => {
                this.save();
              }}
            >
              Save
            </button>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default FormInput;
