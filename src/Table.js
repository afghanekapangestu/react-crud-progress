import React from "react";
import { Table, Container, Row, Col } from "reactstrap";

function ViewTable(props) {
  return (
    <Container>
      <Row>
        <Col>
          <h2>{props.nama}</h2>
          <Table striped>
            <thead>
              <tr>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Jenis Kelamin</th>
              </tr>
            </thead>

            <tbody>
              {props.data.map((x, i) => {
                return (
                  <tr>
                    <td>
                      {i}. {x.nama}
                    </td>
                    <td>{x.alamat}</td>
                    <td>{x.jenis_kelamin}</td>
                    <td>
                      <button
                        className="btn btn-outline-warning"
                        onClick={e => {
                          props.onSelectEdit(x);
                        }}
                      >
                        Edit{" "}
                      </button>
                      &nbsp;
                      <button
                        onClick={e => {
                          props.onDelete(x.id);
                        }}
                        className="btn btn-outline-danger"
                      >
                        Delete
                      </button>
                      &nbsp;
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  );
}

export default ViewTable;
